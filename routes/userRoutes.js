const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const orderControllers = require('../controllers/orderControllers')
const auth = require('../auth')
const {verify , verifyAdmin} = auth

const {
	userRegister,
	userLogin,
	userSetAdmin,
	userDetails
} = userControllers


router.post('/register', userRegister)

router.post('/login', userLogin)

router.get('/details', verify, userDetails);

module.exports = router

