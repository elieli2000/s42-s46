const express = require('express')
const router = express.Router()
const orderControllers = require('../controllers/orderControllers')
const auth = require('../auth')
const {verify, verifyAdmin, verifyNonAdmin} = auth

const {
	createOrder,
	retrieveAllOrder,
	retrieveAuthOrder
} = orderControllers

router.post('/create', verify, verifyNonAdmin, createOrder)

router.get('/all',verify, verifyAdmin, retrieveAllOrder)

router.get('/retrieveAuthOrder', verify, verifyNonAdmin, retrieveAuthOrder)

module.exports = router

