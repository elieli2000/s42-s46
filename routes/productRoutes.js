const express = require('express')
const router = express.Router()
const productControllers = require('../controllers/productControllers')
const auth = require('../auth')
const {verify, verifyAdmin} = auth

const {
	createProduct,
	allActiveProducts,
	getSingleProduct,
	updateProduct,
	productArchive
} = productControllers


router.post('/createProduct', verify, verifyAdmin, createProduct)

router.get('/allActiveProducts', allActiveProducts)

router.get('/:id', verify, getSingleProduct)

router.put('/:id', verify, verifyAdmin, updateProduct)

router.put('/archive/:id', verify, verifyAdmin, productArchive)

module.exports = router