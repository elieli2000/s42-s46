

const Product = require('../models/product')

// Create Product
module.exports.createProduct = (req, res) => {
	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	})
	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error))
};

// Retrieve all active product
module.exports.allActiveProducts = (req, res) => {
	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.status(500).send)
};

// Retrieve single product
module.exports.getSingleProduct = (req, res) => {
	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
};

// Update Product information
module.exports.updateProduct = (req, res) => {
	let updatedProduct = {
		name:req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive
	}
	Product.findByIdAndUpdate(req.params.id, updatedProduct, {new:true})
	.then(result => res.send('Product updated successfully.'))
	.catch(error => res.send('Unable to find product ID.'))
}

// Archive Product
module.exports.productArchive = (req, res) =>{
	let updatedProduct = {
		isActive: false
	}
	Product.findByIdAndUpdate(req.params.id, updatedProduct, {new:true})
	.then(result => res.send('Product moved to archive.'))
	.catch(error => res.send(error))
}

