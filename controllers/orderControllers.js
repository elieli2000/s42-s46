const Order = require('../models/order')
const User = require('../models/user')
const Product = require('../models/product')

module.exports.createOrder = async (req,res) => {
	try {
		const product = await Product.findOne({productId: req.body.productId})
		const newOrder = new Order ({
			userId: req.body.userId,
			products: {
				productId: req.body.productId,
				quantity: req.body.quantity
			},
			totalAmount: product.price * req.body.quantity
		})
		await newOrder.save()
		return res.send({newOrder, message: "Order submitted"})
	} catch(error) {
		return res.send(error.message)
	}
}

module.exports.retrieveAllOrder = (req,res) => {
	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.retrieveAuthOrder = (req,res) => {
	Order.find({userId: req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
