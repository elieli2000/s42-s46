const User = require('../models/user')
const Order = require("../models/order")
const Product = require("../models/product")
const bcrypt = require('bcrypt')
const auth = require('../auth')
const {createAccessToken} = auth

// User registration
module.exports.userRegister = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result !==null && result.email === req.body.email) {
			return res.send('Email already taken.')
		} else {

			if(req.body.password.length < 8) {
				return res.send({message: "Password is too short"})
			}  

				let newUser = new User({
					email: req.body.email,
					password: bcrypt.hashSync(req.body.password,10),
					isAdmin: req.body.isAdmin
				})
					return newUser.save().then((user, error) => {
					console.log(newUser)
						if(error) {
							return console.error(error)
						} else {
						return res.send("Registration successful")
						}
					})
		}
	})
};


// User Authentication
module.exports.userLogin = (req,res) => {
	User.findOne({email: req.body.email})
	.then(result => {
		if(result === null){
			return res.send({message:"No User Found."})	
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,result.password)
			console.log(isPasswordCorrect)

			if(isPasswordCorrect){
				return res.send({accessToken: createAccessToken(result)})
			}
			else{
				return res.send({message: "Password is incorrect."})
			}
		}
	})
	.catch(error => res.send(error))
}


// Retrive user details
module.exports.userDetails = (req,res) => {
	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

