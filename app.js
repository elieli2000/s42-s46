// Express Setup
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express('app');
const port = process.env.PORT || 4000;

// Middlewares:
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));



const userRoutes = require('./routes/userRoutes')
app.use('/users', userRoutes)


const productRoutes = require('./routes/productRoutes')
app.use('/products', productRoutes)

const orderRoutes = require('./routes/orderRoutes')
app.use('/orders', orderRoutes)


// Mongoose connection
mongoose.connect(`mongodb+srv://admin:admin123@cluster0.evvkhyz.mongodb.net/s42-46?retryWrites=true&w=majority`, {

	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection

db.on('error', () => console.error('Connection Error'));
db.once('open', () => console.log('Connected to MongoDB!'));


app.listen(port, () => console.log(`API is now online at port: ${port}`));


