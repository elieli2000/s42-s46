const jwt = require('jsonwebtoken');
const secret = "EcommerceAPI";

module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data,secret,{});
}

module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);
		jwt.verify(token,secret,function(error, decodedToken){
			if(error){
				return res.send({
					auth:"Failed",
					message: error.message
				})
			} else {
				console.log(decodedToken);
				req.user = decodedToken;
				next()
			}
		})
	} else {
		return res.send({auth:"Failed. No Token"});
	}
};


module.exports.verifyAdmin = (req, res, next) => {
	if(req.user.isAdmin){
		next()
	} else {
		return res.send({
			auth: "Failed: Admin Token doesn't exist",
			message: "Not Authorize."

		})
	}
}

module.exports.verifyNonAdmin = (req, res, next) => {
	if(req.user.isAdmin === false){
		next()
	} else {
		return res.send({

			auth: "Failed: Client Token doesn't exist",
			message: "Action Forbidden"

		})
	}
}


